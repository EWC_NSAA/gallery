﻿using System.Web.Mvc;
using Gallery.Code;

namespace Gallery.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "About";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Contact";

			return View();
		}

		public ActionResult Photos(int id)
		{
			
			string folder = "~/photos/";
			switch (id)
			{
				case 0:
					folder = "~/photos/";
					break;
				case 1:
					folder = "~/photos/cameras/";
					break;
				case 2:
					folder = "~/photos/cars/";
					break;
				case 3:
					folder = "~/photos/ships/";
					break;
			}

			return View(new PhotoModel(folder));
		}
	}
}